from django.conf.urls import patterns, include, url
from pinche import views

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
                       (r'^car-form/$', views.car_form),
                       (r'^car-index/$', views.car_index),
                       (r'^car-index/page-(\d+)/$', views.car_index),
                       (r'^car-detail/car-id-(\d+)/$', views.car_detail),
                       (r'^car-search/$', views.car_search),
                       (r'^car-join-success/$', views.car_join_success),
    # Examples:
    # url(r'^$', 'ECUSTLife.views.home', name='home'),
    # url(r'^ECUSTLife/', include('ECUSTLife.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
