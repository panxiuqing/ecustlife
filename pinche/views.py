# -*- coding: utf-8 -*-

# Create your views here.
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from pinche.models import Car
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import datetime

def car_form(request):
    error = success = False
    post_data = {}
    car_db_keys = {'begin': 30, 'end': 30, 'your_name': 30, 'phone': 20, 'qq': 10, 'remark': 100}
    range_hour = range(0, 24)
    range_minute = range(0, 60, 10)
    message = {"main": ""}

    if request.method == 'GET':
        return render_to_response('car-form.html', {'range_hour': range_hour, 'range_minute': range_minute, 'message': message})
    else:
        for key in car_db_keys.keys():
            post_data[key] = request.POST[key]
            if len(request.POST[key]) > car_db_keys[key]:
                message[key] = "字符个数过多"
                error = True
    if error:
        message['main'] = "表单填写有误，请检查后再次提交。"
        return render_to_response('car-form.html', {'range_hour': range_hour, 'range_minute': range_minute, 'message': message, 'error': error, "post_data": post_data})
    else:
        Car.objects.create(begin = post_data['begin'],
                           end = post_data['end'],
                           your_name = post_data['your_name'],
                           qq = request.POST['qq'],
                           phone = post_data['phone'],
                           date = request.POST['date'],
                           time = request.POST['time_hour'] + ':' + request.POST['time_minute'],
                           people_total = request.POST['people_total'],
                           people_already = 1,
                           remark = post_data['remark'] ).save()
        message['main'] = "您已成功发起一次拼车，请静侯佳音！"
        return render_to_response('car-form.html', {'range_hour': range_hour, 'range_minute': range_minute, 'message': message, 'error': error})

def car_index(request, offset='1'):
    #delete time-out items
    now = datetime.datetime.now()
    for car in Car.objects.all():
        if car.date < now.date():
            car.delete()
        else:
            if car.date == now.date() and car.time < now.time():
                car.delete()
                
    Car_objects_all = Car.objects.all()
    pages = Paginator(Car_objects_all, 10)

    #generate a typeahead list for frontend
    begin_list = []
    for car in Car_objects_all:
        if car.begin not in begin_list:
            begin_list.append(car.begin)

    #deal with the pages
    try:
        page_num = int(offset)#request.GET['page'] if request.GET.keys() else 1
    except ValueError:
        raise Http404()

    pages_range = range(1, 1 + pages.num_pages)
    try:
        page = pages.page(page_num)
    except PageNotAnInteger:
        page = pages.page(1)
    except EmptyPage:
        page = pages.page(pages.num_pages)    
    #page_previous = "" if page.has_previous() else "disabled"
    #page_next = "" if page.has_next() else "disabled"

    site_url = '/car-index'
    return render_to_response('car-index.html', {'cars': page, 'pages_range': pages_range, 'SITEURL': site_url})

#显示拼车详细信息
def car_detail(request, offset):
    #i = request.GET['id']
    try:
        i = int(offset)
    except ValueError:
        raise Http404()
    car = Car.objects.get(id=i)
    return render_to_response('car-detail.html', {'car': car})

#成功加入拼车后的函数
def car_join_success(request):
   # try:
   #     num = int(offset)
   # except ValueError:
   #     raise Http404()
    i = request.GET['id']
    car = Car.objects.get(id=i)
    if car.people_total > car.people_already:
        car.people_already += 1
    car.save()
    #if car.people_total > car.people_already:
    #    status = ["还有", car.people_total - car.people_already, "空位，快邀请好友加入吧，记住本车 id = ", car.id, "!"]
    #else:
    #   status = ["人数已满"]
    return render_to_response('car-join-success.html')


def car_search(request):
    search_begin = request.POST['search-begin']
    search_end = request.POST['search-end']
    if request.POST['search-id']:
        search_id = request.POST['search-id']
        pages = Paginator(Car.objects.filter(begin__contains=search_begin,end__contains=search_end,id=search_id), 100)
    else:
        pages = Paginator(Car.objects.filter(begin__contains=search_begin,end__contains=search_end), 100)
    return render_to_response('car-index.html', {'cars': pages.page(1), 'page_previous': 'disabled', 'page_next': 'disabled'})
