from django.db import models

# Create your models here.
class Car(models.Model):
    begin = models.CharField(max_length=30)
    end = models.CharField(max_length=30)
    your_name = models.CharField(max_length=30)
    qq = models.CharField(max_length=10)
    phone = models.CharField(max_length=20)
    date = models.DateField()
    time = models.TimeField()
    people_total = models.IntegerField()
    people_already = models.IntegerField()
    remark = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        ordering = ['date']
